<?php

namespace Drupal\task\Service;

use GuzzleHttp\Exception\GuzzleException;

/**
 * Interface ApiGetContentsInterface
 */
interface GetApiContentsInterface
{

  /**
   * API Get Contents
   *
   * @param bool|string $apiUrl
   * @param bool|string $apiEndpoint
   * @param bool|string $apiToken
   * @return mixed
   */
  public function apiGetContents(string $apiUrl, string $apiEndpoint, string $apiToken
  );

}

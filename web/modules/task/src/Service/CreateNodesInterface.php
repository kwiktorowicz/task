<?php

namespace Drupal\task\Service;

use Drupal\node\Entity\Node;

/**
 * Interface CreateNodesInterface.
 */
interface CreateNodesInterface {

  /**
   * Get new contents to create
   *
   * @return array
   */
  public function getContentsToCreate(): array;

  /**
   * Create nodes entities
   */
  public function createNodes();
}

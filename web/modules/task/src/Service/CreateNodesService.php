<?php

namespace Drupal\task\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use GuzzleHttp\Exception\GuzzleException;

/**
 * CreateNodes service.
 */
class CreateNodesService
{

  /**
   * @var EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * @var EntityTypeManager
   */
  private $apiGetContents;

  /**
   * @param EntityTypeManager $entityTypeManager
   * @param GetApiContentsService $apiGetContents
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function __construct(EntityTypeManager $entityTypeManager, GetApiContentsService $apiGetContents)
  {
    $this->entityTypeManager = $entityTypeManager;
    $this->apiGetContents = $apiGetContents;
  }

  /**
   * Create Nodes
   */
  public function getContentsToCreate(): array
  {
    $getContents = $this->apiGetContents->apiGetContents();

    $contentsToCreate = [];
    if (isset($getContents) && $getContents->entries) {
      foreach ($getContents->entries as $entry) {
        $loadNodeEntity = $this->entityTypeManager->getStorage('node')->loadByProperties(
          ['field_id' => $entry->_id]
        );
        if (!$loadNodeEntity) {
          $contentsToCreate[] = $entry;
        }
      }
    }

    return $contentsToCreate;
  }

  /**
   * Create Nodes
   *
   * @throws EntityStorageException
   */
  public function createNodes()
  {
    $contentsToCreate = $this->getContentsToCreate();
    if (!empty($contentsToCreate)) {

      foreach ($contentsToCreate as $entry) {

        $node = Node::create([
          'type' => 'task',
          'created' => $entry->_created ?: time(),
          'changed' => $entry->_modified ?: time(),
          'title' => $entry->title ?: '',
          'field_id' => $entry->_id ?: '',
          'field_description' => $entry->description ?: '',
          'field_gallery_path' => $entry->gallery ? $entry->gallery->path : '',
        ]);
        $node->field_description->format = 'full_html';
        $node->save();
      }
    }
  }
}

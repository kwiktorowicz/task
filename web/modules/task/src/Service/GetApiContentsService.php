<?php

namespace Drupal\task\Service;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApiGetContents
 */
class GetApiContentsService implements GetApiContentsInterface
{

  /**
   * GuzzleHttp\Client definition.
   *
   * @var ClientInterface
   */
  protected $httpClient;


  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('http_client')
    );
  }


  /**
   * @param ClientInterface $httpClient
   */
  public function __construct(
    ClientInterface $httpClient
  )
  {
    $this->httpClient = $httpClient;
  }


  /**
   * API Get Contents
   *
   * @param string $apiUrl
   * @param string $apiEndpoint
   * @param string $apiToken
   * @return mixed
   */
  public function apiGetContents(
    string $apiUrl = '',
    string $apiEndpoint = '',
    string $apiToken = ''
  ){

    $apiSettings = \Drupal::config('task.api_settings');
    if(empty($apiUrl)) {
      $apiUrl = $apiSettings->get('api_url');
    }
    if(empty($apiEndpoint)) {
      $apiEndpoint = $apiSettings->get('api_endpoint');
    }
    if(empty($apiToken)) {
      $apiToken = $apiSettings->get('api_token');
    }

    $request = $this->httpClient->request(
      'GET',
      $apiUrl . $apiEndpoint . '?token=' . $apiToken, [
        'curl' => [
          CURLOPT_RETURNTRANSFER => true,
        ],
        'headers' => [
          "token" => $apiToken,
        ],
        'form_params' => Null
      ]
    );

    if ($request->getStatusCode() == 200) {
      return json_decode($request->getBody()->getContents());
    } else {
      return null;
    }
  }

}

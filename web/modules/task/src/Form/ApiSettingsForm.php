<?php

namespace Drupal\task\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure task settings for this site.
 */
class ApiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'task_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['task.api_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api url'),
      '#default_value' => $this->config('task.api_settings')->get('api_url'),
      '#description' => $this->t('example: https://inprogra.dev/test/api'),
      '#required' => true,
    ];
    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api endpoint'),
      '#default_value' => $this->config('task.api_settings')->get('api_endpoint'),
      '#description' => $this->t('example: /collections/get/posts'),
      '#required' => true,
    ];
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api token'),
      '#default_value' => $this->config('task.api_settings')->get('api_token'),
      '#description' => $this->t('example: 819073dff67f75f1b6ce6f84fe4fcb'),
      '#required' => true,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Validate Api url - empty
    if (empty($form_state->getValue('api_url'))) {
      $form_state->setErrorByName('api_url', $this->t('Field "api_url": The value is empty.'));
    }

    // Validate Api url - url
    if (!filter_var($form_state->getValue('api_url'), FILTER_VALIDATE_URL)) {
      $form_state->setErrorByName('api_url', $this->t('Field "api_url": The value is not correct.'));
    }

    // Validate Api endpoint - empty
    if (empty($form_state->getValue('api_endpoint'))) {
      $form_state->setErrorByName('api_endpoint', $this->t('Field "api_endpoint": The value is empty'));
    }

    // Validate Api token - empty
    if (empty($form_state->getValue('api_token'))) {
      $form_state->setErrorByName('api_token', $this->t('Field "api_token": The value is empty'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('task.api_settings')
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('api_token', $form_state->getValue('api_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}

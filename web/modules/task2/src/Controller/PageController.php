<?php

namespace Drupal\task2\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for task2 routes.
 */
class PageController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function getPage() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works, user is authorized!'),
    ];

    return $build;
  }

}
